#include <iostream>
#include <cstring>
#include <errno.h>
#include <stdio.h>


#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
 
using namespace std;
 
#define  PATH_NAME "temp"
 
int main()
{
    int *memPtr;
    int fd;
 
    fd = open(PATH_NAME, O_RDWR);
    if (fd < 0)
    {
        cout<<"open file "<<PATH_NAME<<" failed...";
        cout<<strerror(errno)<<endl;
        return -1;
    }
 
    memPtr = (int *)mmap(NULL, 8192, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
 
    if (memPtr == MAP_FAILED)
    {
        cout<<"mmap failed..."<<strerror(errno)<<endl;
        return -1;
    }
    
	for(int i=0;i<26;i++,memPtr=memPtr+1){
		printf("read:%c at:%p\n",*memPtr,memPtr);
    }
    cout<<"process:"<<getpid()<<endl;
	munmap(memPtr-26,8192);
	if (memPtr == MAP_FAILED)
    {
        cout<<"munmap failed..."<<strerror(errno)<<endl;
        return -1;
    }
    return 0;
}
