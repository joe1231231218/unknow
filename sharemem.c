#include "sharemem.h"

#include <string.h>
#include <errno.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define  PATH_NAME "temp"

size_t FileSizeK;

char *MemPtr,*MemEltb,*MemElem;     

enum element_type_enum element_type_enum_entity;  

int create_new_file(size_t kbyte){
	char buff[1024]={0};
	
    FILE *fd;
	struct segment_header hd;
	
	hd.magic_number[0]='e';
	hd.magic_number[1]='r';
	hd.magic_number[2]='m';
	hd.magic_number[3]='e';
	hd.magic_number[4]='m';
	hd.write_lock=0;
	hd.pid=getpid();
	hd.capacity=kbyte;
	hd.access_control=7;
	
    fd = fopen(PATH_NAME,"rw");
    if (fd == NULL)
    {
        printf("open file %s failed...",PATH_NAME);
        return -1;
    }
	fwrite(buff,sizeof(buff),kbyte,fd);
	fseek(fd,0,SEEK_SET);
	fwrite((void*)&hd,sizeof(struct segment_header),1,fd);
	fclose(fd);
	
	fid = open(PATH_NAME,O_RDWR);
    memPtr = (int *)mmap(NULL, 1024*kbyte, PROT_READ | PROT_WRITE, MAP_SHARED, fid, 0);
    if(fid == -1){
		printf("mmap fial . . .");
	}
    close(fid);
	
	fid=msync(memPtr,1024*kbyte,MS_SYNC);
    if (fid == -1)
    {
        printf("msync failed...");
        return -1;
    }
    
	FileSizeK=1024*kbyte;
	
	return 0;
}

int open_file(){
	int fid;
	char *memPtr;
	FILE *fd;
	struct segment_header hd;
	
	fd = fopen(PATH_NAME,"rw");
    if (fd == NULL)
    {
        printf("open file %s failed...",PATH_NAME);
        return -1;
    }
	fread(&hd,sizeof(struct segment_header),1,fd);
	FileSizeK=hd.capacity;
	printf("%s FileSizeK:%ld\n",PATH_NAME,FileSizeK);
	fclose(fd);
	
	fid = open(PATH_NAME,O_RDWR);
    memPtr = (char *)mmap(NULL, 1024*FileSizeK, PROT_READ | PROT_WRITE, MAP_SHARED, fid, 0);
    if(fid == -1){
		printf("mmap fial . . .");
	}
    close(fid);
    MemPtr=memPtr;
    MemEltb=(memPtr+sizeof(struct segment_header));
    MemElem=memPtr+(sizeof(struct segment_header)+sizeof(struct element_table_u)*element_table_len);
	return 0; 
}
void write_ring(struct element_ring *el,char *d,size_t len){
	for(size_t i = 0;i<len;++i){
		*(el->data+(char*)el->producer_offset) = d[i];
		if(i%el->size == 0){
			el-> producer_offset=rl->data;
		}
	}
	return;
}
void read_ring(struct element_ring *el,char *d ,size_t len){
	d=(char*)malloc(sizeof(char)*len);
	for(size_t i = 0;i<len;++i){
		d[i]=*(el->data+(char*)el->consumer_offset);
		if(i%el->size == 0){
			el->consumer_offset=rl->data;
		}
	}
	return;
}
void* find_putable_Elem(size_t len){
	
	
}
void* find_sys()
void* read_var(size_t ln){
	struct element_table_u *tu=element_table_at(ln);
	if(tu->element == 0){
		return NULL;
	}
	struct element_number *el=(struct element_number*)(MemPtr+tu->element);
	if(el->element_type == Integer || el->element_type == floating_point){
		el->read_flag=0;
		element_type_enum_entity=el->element_type;
		return el;
	}else if(el->element_type == Integral_array || el->element_type == float_array){
		struct element_array *elr=(struct element_array*)(MemPtr+tu->element);
		elr->read_flag=0;
		element_type_enum_entity=el->element_type;
		return elr;
	}else if(el->element_type == buffer){
		element_type_enum_entity=el->element_type;
		return el;  
	}
	
	return NULL;
}

void* write_var(size_t ln,void * ptr,enum element_type_enum element_type){
	struct element_table_u *tu=element_table_at(ln);
	if(tu->element == 0){
		if(element_type == Integer || element_type == floating_point){
			void *np = find_putable_Elem(sizeof(struct element_number));
			*(struct element_number*)np = *(struct element_number*)ptr;
			tu->element=MemPtr-np;
		}else if(element_type == Integral_array || element_type == float_array){
			void *np = find_putable_Elem(sizeof(struct element_array)+sizeof(int)*((struct element_array*)ptr->size));
			*(struct element_array*)np = *(struct element_array)ptr;
			tu->element=MemPtr-np;
		}else if(element_type == buffer){
			void *np = find_putable_Elem(sizeof(struct element_ring)+sizeof(int)*((struct element_ring*)ptr->size));
			*(struct element_array*)np = *(struct element_array)ptr;
			tu->element=MemPtr-np;
		}
		
	}else if(tu->element_type == Integer || tu->element_type == floating_point){
		if(element_type == tu->element_type){
			(struct element_number*)(tu->element)->read_flag=0;
			(struct element_number*)(tu->element)->num=(struct element_number*)ptr->num;
		}else{
			(struct element_number*)(tu->element)->element_type = 0;
			void *np = find_putable_Elem(sizeof(struct element_number);
			*(struct element_number*)np = *(struct element_number*)ptr;
			tu->element=MemPtr-np;
		}
		return tu->element;
	}else if(el->element_type == Integral_array || el->element_type == float_array){
		if(element_type == tu->element_type){
			(struct struct element_array*)(tu->element)->read_flag=0;
			(struct struct element_array*)(tu->element)->num=(struct element_array*)ptr->num;
		}else{
			(struct element_array*)(tu->element)->element_type = 0;
			void *np = find_putable_Elem(sizeof(struct element_array)+sizeof(int)*((struct element_array*)ptr->size));
			*(struct element_array*)np = *(struct element_array*)ptr;
			tu->element=MemPtr-np;
		}
		return tu->element;
	}else if(el->element_type == buffer){
		if(element_type == tu->element_type){
			(struct struct element_ring*)(tu->element)->read_flag=0;
			(struct struct element_ring*)(tu->element)->num=(struct element_ring*)ptr->num;
		}else{
			(struct element_ring*)(tu->element)->element_type = 0;
			void *np = find_putable_Elem(sizeof(struct element_ring)+sizeof(int)*((struct element_ring*)ptr->size));
			*(struct element_ring*)np = *(struct element_ring*)ptr;
			tu->element=MemPtr-np;
		}
		return tu->element;
	}
}
int read_syn(){
	struct element_table_u *tu=(struct element_table_u*)&(MemEltb[ln]);
	if(tu->element != 0 && ((element_number *)tu->element)->element_type <=2){
		
	}
	if(el->element_type == Integer || el->element_type == floating_point){
		el->read_flag=0;
		element_type_enum_entity=el->element_type;
		return el;
	}else if(el->element_type == Integral_array || el->element_type == float_array){
		struct element_array *elr=(struct element_array*)(MemPtr+tu->element);
		elr->read_flag=0;
		element_type_enum_entity=el->element_type;
		return elr;
	}else if(el->element_type == buffer){
		struct element_ring = (struct element_ring*)(MemPtr+tu->element);
		element_type_enum_entity=el->element_type;
		return el;  
	}
}

int main(){
	create_new_file(1); 
	open_file();

	return 0;
}
