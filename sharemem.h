#define SHARE_MEM
#define element_table_len 4096
#define offset_lenth 12
#define element_table_at(x) (struct element_table_u*)element_table[(x)]
enum element_type_enum{Integer=1,floating_point,Integral_array,float_array,string,buffer}; //3 bit

//enum element_type_enum element_type_enum_entity;

struct segment_header{
	unsigned char  magic_number[5];
	unsigned int  write_lock: 32;
	unsigned int  :0;
	unsigned int  pid: 16;
	unsigned int  capacity: 16;
	unsigned int  :0;
	unsigned int  access_control: 3; // read write delete
};

struct element_table_u{
	unsigned int  symbol: offset_lenth;
	unsigned int  element: offset_lenth;
};

typedef struct element_table_u element_table[element_table_len];

struct element_number{
	unsigned int  element_type: 3;
	unsigned int  read_flag: 1;
	unsigned int  reverse: 28;                    // fit to 64 bit
	union Num{int i;double d;} num;
};

struct element_array{
	unsigned int  element_type: 3;
	unsigned int  size: 8;
	unsigned int  read_flag: 1;
	unsigned int  reverse: 20;
	union Data{int *i;double *d;} data;
};

struct element_ring{
	unsigned int  element_type: 3;
	unsigned int  size: 8;
	unsigned int  producer_offset: offset_lenth;
	unsigned int  consumer_offset: offset_lenth;
	union Data data;
};
